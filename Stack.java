class Node {
    public int value;
    public Node next;

    public Node(int value) {
        this.value = value;
    }
}


/**
 * Stack implementation based on linked list of Node
 */
class Stack {
    private Node top;

    public Stack(int[] initializer) {
        for (int i: initializer) {
            Node newNode = new Node(i);
            newNode.next = top;
            top = newNode;
        }
    }

    public void push(Node n) {
        n.next = top;
        top = n;
    }

    public void push(int i) {
        push(new Node(i));
    }

    public Node pop() {
        if (!isEmpty()) {
            Node returnValue = top;
            top = top.next;
            return returnValue;
        }
        return null;
    }

    public boolean isEmpty() {
        return top == null;
    }
}

class Main {
    public static void main(String[] args) {
        Stack s = new Stack(new int[] {1,3,4,6,8});
        s.push(2);
        s.push(new Node(13));

        while (!s.isEmpty()) {
            System.out.println(s.pop().value);
        }
    }
}