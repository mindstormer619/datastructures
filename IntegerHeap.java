import java.util.Arrays;

/**
 * Integer Max Heap implementation, with int[] as the underlying data structure.
 */
class IntegerHeap {
    private int[] heapImpl;
    private int size;

    /**
     * Creates a new IntegerHeap (maxheap implementation).
     * @param initializer Array containing initial set of values.
     */
    public IntegerHeap(int[] initializer) {
        this.heapImpl = initializer;
        this.size = initializer.length;
        heapifyAll();
    }
    
    /**
     * Checks if heap is empty.
     * @return true if heap is empty
     */
    public boolean isEmpty() {
        return size <= 0;
    }
    
    /**
     * Peeks at topmost value of heap.
     * @return Topmost value (maximum value in heap)
     */
    public int peek() {
        return heapImpl[0];
    }
    
    /**
     * Pops off the top value of the heap, reducing its size by 1.
     * @return Topmost value (maximum value in heap)
     */
    public int pop() {
        int result = peek();
        swap(0, size-1);
        size -= 1;
        if (size > 0) heapify(0);
        return result;
    }
    
    /**
     * Heapifies all non-leaf nodes. Leaf nodes are valid heaps trivially.
     */
    private void heapifyAll() {
        for (int i = size/2 - 1; i >= 0; --i) {
            heapify(i);
        }
    }

    /**
     * Heapifies a tree starting at given node index.
     * @param node Index to start heapification.
     */
    private void heapify(int node) {
        int left = node * 2 + 1;
        int right = node * 2 + 2;

        int largest = findLargest(node, left, right);
        if (node != largest) {
            swap(node, largest);
            heapify(largest);
        }
    }

    /**
     * Finds the index of the node with the largest value among
     * given node indices a,b,c.
     * 
     * If any of the given indices do not exist, they are treated
     * as the smallest values.
     */
    private int findLargest(int a, int b, int c) {
        int largest = findLargest(a, b);
        largest = findLargest(largest, c);
        return largest;
    }

    /**
     * Returns the index of the node with the largest value among
     * given node indices a,b.
     * 
     * Invalid node is treated as smallest integer value.
     */
    private int findLargest(int a, int b) {
        if (a >= size) {
            return b;
        }
        if (b >= size) {
            return a;
        }
        return (heapImpl[b] > heapImpl[a]) ? b : a;
    }

    /**
     * Swaps the values of two indices in the heap array.
     * Validates that the indices are present within the array.
     * 
     * @param a First index
     * @param b Second index
     */
    private void swap(int a, int b) {
        if (a >= size || b >= size) return;
        int temp = heapImpl[a];
        heapImpl[a] = heapImpl[b];
        heapImpl[b] = temp;
    }


    public static void main(String[] args) {
        int[] initializer = {3,2,7,4,9,1,5,8};
        IntegerHeap heap = new IntegerHeap(initializer.clone());

        while (!heap.isEmpty()) {
            System.out.println(heap.pop());
        }

        System.out.println(Arrays.toString(initializer));
    }
}